// Wrap ClientIP elements, add queue to simulate true Client IO

elementclass ClientIPWrapper{ IP $host_ip, LINKNUM $lkn, CAPACITY $capacity|

ip :: ClientIP(MY_ADDRESS $host_ip, LINK_NUM $lkn);

in_que :: Queue($capacity);
out_que ::  Queue();

input[0]->[0]in_que[0]->[0]ip[0]->out_que[0]->Unqueue->[0]output;
out_que[1]->Discard();

// Wrap the pack from TCP with IP header
input[1]->IPWrapTCP()-> [1]ip[1] -> [1]output;

}
