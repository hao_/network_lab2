// Wrap the RouterIP element adding queue, to simulate congestion

elementclass RouterIPWrapper{ IP $host_ip, LINKNUM $lkn, CAPACITY $capacity  |

ip :: RouterIP(MY_ADDRESS $host_ip, LINK_NUM $lkn);

que :: Queue(CAPACITY $capacity);
out_que ::  Queue();

input[0]->[0]que[0]->[0]ip[0]->out_que[0]->Unqueue->[0]output;
que[1]->Print("Congestion occur")->Discard();

}
