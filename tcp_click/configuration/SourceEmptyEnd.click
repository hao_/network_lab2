
elementclass SourceEmptyEnd{ DATA $data, RATE $rate, LIMIT $limit |

dataque :: Queue($limit);
RatedSource(DATA $data, RATE 1000000, LIMIT $limit) -> dataque;
RatedSource(DATA "", RATE 1, LIMIT 1) -> DelayShaper(1) -> Unqueue-> dataque;

dataque-> RatedUnqueue(RATE $rate) ->[0]output;

}
