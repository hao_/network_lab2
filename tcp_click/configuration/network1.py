import NetBuilder as nb


r1 = nb.Router()
r2 = nb.Router()
c1 = nb.Client()
c2 = nb.Client()

nb.link(r1, r2, 0.995, 0.01)
nb.link(c1, r1, 0.995, 0.01)
nb.link(c2, r2, 0.995, 0.01)


c1.add_app("SourceEmptyEnd(DATA \"helloA\", RATE 100, LIMIT 300)", c2, 22, 80)
#c1.add_app("SourceEmptyEnd(DATA \"helloB\", RATE 100, LIMIT 10)", c2, 23, 81)

nb.build()
