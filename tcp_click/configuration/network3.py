import NetBuilder as nb


r1 = nb.Router()
r2 = nb.Router()
cL1 = nb.Client()
cL2 = nb.Client()
cR1 = nb.Client()
cR2 = nb.Client()

nb.link(r1, r2, 0.995, 0.01)
nb.link(cL1, r1, 0.998, 0.01)
nb.link(cL2, r1, 0.998, 0.01)
nb.link(cR1, r2, 0.998, 0.01)
nb.link(cR2, r2, 0.998, 0.01)


cL1.add_app("SourceEmptyEnd(DATA \"helloA\", RATE 100, LIMIT 500)", cR1, 22, 80)
cL2.add_app("SourceEmptyEnd(DATA \"helloB\", RATE 100, LIMIT 500)", cR2, 23, 81)

nb.build()
