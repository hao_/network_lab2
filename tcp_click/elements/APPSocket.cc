#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh> 
#include <click/timer.hh>
#include "APPSocket.hh"
#include "TCPHeader.hh"
#include "header_helper.cc"
#include "helper.hh"

CLICK_DECLS 

#define CLOSED (0)
#define CONNECTED (1)
#define CONNECTING (2)

APPSocket::APPSocket() : _timer_pull(this), _timer_close(this) {
	_state = CLOSED;
}

APPSocket::~APPSocket(){	
}

int APPSocket::configure(Vector<String> &conf, ErrorHandler *errh) {
	String src_ip, dst_ip;
	if (cp_va_kparse(conf, this, errh,
					"SRC_IP", cpkM, cpString, &src_ip,
					"DST_IP", cpkM, cpString, &dst_ip,
					"SRC_PORT", cpkM, cpUnsigned, &_src_port,
					"DST_PORT", cpkM, cpUnsigned, &_dst_port,
					"LISTEN", cpkM, cpUnsigned, &_listen,
					cpEnd) < 0) {
		return -1;
	}
	_src_ip = translateIP(src_ip.c_str());
	_dst_ip = translateIP(dst_ip.c_str());
	return 0;
}

int APPSocket::initialize(ErrorHandler*){
	if (_listen) {
		click_chatter("APP: listen TCP connection");
		_state = CONNECTING; 
		TryToConnect(TCP_LISTEN);
	}
    _timer_pull.initialize(this);
    _timer_pull.schedule_now();
    _timer_close.initialize(this);
    _try_time = 0;
    return 0;
}

void APPSocket::push_TCPoutput(Packet *packet) {
	TCPHeader *header = header_get<TCPHeader>(packet);

	if (header->type == TCP_CLOSE) {
		if (_state == CONNECTED) {
			click_chatter("APP: connection closed");
		}
		_state = CLOSED;
		_listen = 1;
		packet->kill();
	} else if (header->type == TCP_CONNECT) {
		click_chatter("APP: connected success");
		_state = CONNECTED;
		packet->kill();
	} else if (header->type == TCP_ASK_TO_CLOSE) {
		click_chatter("APP: peer closed, passively close");
		header->type = TCP_CLOSE;
		output(0).push(packet);
	}
	else if (header->type == TCP_DATA) {
		output(1).push(packet);
		//Packet *inner = header_unpack<TCPHeader>(packet);
		//output(1).push(inner);
		//packet->kill();
	} else {
		click_chatter("APP get wrong TCP type");
	}
}

void APPSocket::TryToConnect(uint8_t TCP_TYPE) {
	Packet *packet = Packet::make(40,0, sizeof(TCPHeader), 28);

	TCPHeader *header = header_get<TCPHeader>(packet);
	header->type = TCP_TYPE;
	header->src_ip = _src_ip;
	header->dst_ip = _dst_ip;
	header->src_port = _src_port;
	header->dst_port = _dst_port;
	header->packet_size = packet->length();

	output(0).push(packet);
}

void APPSocket::send_packet(Packet *packet) {
	Packet *wrap_packet = header_wrap<TCPHeader>(packet);
	packet->kill();

	TCPHeader *header = header_get<TCPHeader>(wrap_packet);
	header->src_ip = _src_ip;
	header->dst_ip = _dst_ip;
	header->src_port = _src_port;
	header->dst_port = _dst_port;
	header->packet_size = wrap_packet->length();

	if (wrap_packet->length() == sizeof(TCPHeader)) {
		//Close connection
		header->type = TCP_CLOSE;
		if (_state == CLOSED) {
			wrap_packet->kill();
			return;
		}
		_save_packet = wrap_packet;
		//_timer_close.clear();
		//_timer_close.schedule_after_sec(10);
		output(0).push(wrap_packet);
	} else {
		//Send payload
		header->type = TCP_DATA;
		output(0).push(wrap_packet);
	}
}

void APPSocket::run_timer(Timer *timer)
{
	/*
	if (timer == &_timer_close) {
		click_chatter("APP: finish transmiting, close the connection");
		output(0).push(_save_packet);
	} else*/
	if (timer == &_timer_pull)
	{
		Packet *packet = pull(1);
		while (packet != NULL) {
			push_TCPoutput(packet);
			packet = pull(1);
		}
		if (_state == CONNECTED) {
			_try_time = 0;
			Packet *packet = pull(0);
			while (packet != NULL && _state == CONNECTED) {
				send_packet(packet);
				packet = pull(0);
			}
		}
		else if (_state == CLOSED && _listen == 0) {
			if ( ++_try_time >= 3) {
				click_chatter("APP: Failed to connect after 3 tried");
				return;
			} else {
				click_chatter("APP: try to connect remote client");
				TryToConnect(TCP_CONNECT);
				_state = CONNECTING;
			}
		}
		_timer_pull.reschedule_after_sec(0.1);
	} else {
		click_chatter("Wrong timer");
		assert(false);
	}
}

CLICK_ENDDECLS 
EXPORT_ELEMENT(APPSocket)
