#ifndef CLICK_APPSOCKET_HH 
#define CLICK_APPSOCKET_HH 
#include <click/element.hh>
#include <click/timer.hh>
#include <click/hashtable.hh>
#include <click/error.hh>


CLICK_DECLS
	
class APPSocket : public Element {
    public:
        APPSocket();
        ~APPSocket();

        const char *class_name() const { return "APPSocket";}
        const char *port_count() const { return "1-/1-"; }
        const char *processing() const { return "l/h"; }

        int initialize(ErrorHandler*);
        void run_timer(Timer *timer);
        int configure(Vector<String> &conf, ErrorHandler *errh);

    private:
    	uint8_t _state;
        Timer _timer_pull, _timer_close;
        uint8_t _try_time, _listen;
		uint32_t _src_ip, _dst_ip;
		uint16_t _src_port, _dst_port;
		Packet *_save_packet;

        void send_packet(Packet*);
        void push_TCPoutput(Packet*);
        void TryToConnect(uint8_t);
}; 

CLICK_ENDDECLS
#endif 
