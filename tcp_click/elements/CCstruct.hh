#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh> 
#include <click/timer.hh>
#include <click/config.h>

#define MAX_QUEUE_SIZE 3000
#define RECEIVER_BUFFER_SIZE 50

struct CC_sender_packet{
	Packet *packet;
	Packet *data_buf;
	uint8_t ack_time;
	uint32_t port_no;
	bool to_retransmit;
	Timer *timer;

	CC_sender_packet() {
		packet = NULL;
		ack_time = 0;
		port_no = -1;
		to_retransmit = 0;
		timer = NULL;
	}
};

typedef Packet CC_receiver_packet;

template<class Pack>
struct CC_queue{
	Pack *pack[MAX_QUEUE_SIZE + 5];
	uint32_t LAR, LFS, data_tail;


	//void set(uint32_t seq, Pack* packet);
	Pack*& operator[] (int seq);
	void insert(Pack* packet);
	bool empty();
	Pack*& front();
	void pop();
	void clean();
	uint32_t rest_win();
};

class TCP_congestion;

struct Port_pack_processor{
	CC_queue<CC_sender_packet> send_queue;
	CC_queue<CC_receiver_packet> receive_queue;
	uint32_t send_queue_win_size() {
		return (cwnd > 1) ? cwnd : 1;
	}
	uint32_t receive_queue_win_size() {
		return RECEIVER_BUFFER_SIZE;
	}

	uint8_t state;
	uint32_t seq;
	double cwnd, RTT, time_out;
	bool slow_start, connect;
	bool ready_to_close;

	Timer *timer;
	Packet* create_ACK(uint32_t ack_seq, uint32_t rest_win, TCPHeader* packet_header);
	void send_packet();
	void recv_packet(CC_receiver_packet* packet);
	void retransmit(CC_sender_packet* packet_buff);
	void recv_ack(CC_receiver_packet* packet_ack);
	void packet_from_app(Packet *packet);

	TCP_congestion *elem;
};
