#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh> 
#include <click/timer.hh>
#include "ClientIP.hh"
#include "IPHeader.hh"
#include "PortHeader.hh"
#include "header_helper.cc"
#include "DVPacket.hh"

//#include "helper.cc"
CLICK_DECLS 

ClientIP::ClientIP() : _timer_bocast(this), _timer_pull(this) {
}

ClientIP::~ClientIP(){	
}

int ClientIP::initialize(ErrorHandler*){
    _my_ip = 0;
    const char* str = _my_address.c_str();
    unsigned int tmp = 0, len = strlen(str);
    for (unsigned int i = 0; i < len; ++i)
    	if (str[i] == '.')
    	{
    		_my_ip <<= 8;
    		_my_ip += tmp;
    		tmp = 0;
    	} else tmp = 10 * tmp + str[i] - '0'; 
    _my_ip <<= 8;
    _my_ip += tmp;

    _timer_bocast.initialize(this);
    _timer_bocast.schedule_now();
    _timer_pull.initialize(this);
    _timer_pull.schedule_now();
    return 0;
}


void ClientIP::SendToRouter(Packet *packet){
	Packet* packet_wrap = header_wrap<PortHeader>(packet);
	header_get<PortHeader>(packet_wrap)->port = 0;
	
	output(0).push(packet_wrap);
	packet->kill();
}

void ClientIP::push(int port, Packet *packet) {
	assert(port == 1);
	SendToRouter(packet);
}

int ClientIP::configure(Vector<String> &conf, ErrorHandler *errh) {
	if (cp_va_kparse(conf, this, errh,
				  "MY_ADDRESS", cpkM, cpString, &_my_address,
				  "LINK_NUM", cpkM, cpUnsigned, &_link_num,
				  cpEnd) < 0) {
	return -1;
	}
	//click_chatter("configure : %s %u ", _my_address.c_str(), _link_num);
	return 0;
}

void ClientIP::Broadcast_Table()
{
	WritablePacket *packet = Packet::make(40,0,sizeof(IPHeader) 
					+ sizeof(unsigned int) + sizeof(Entry), 28);

	memset(packet->data(),0,packet->length());

	IPHeader *format = (IPHeader*) packet->data();
	format->src_ip = _my_ip;
	format->dst_ip = -1;
	format->type = IP_ROUNTING;
	format->packet_size = packet->length();
	
	char *p = (char*)packet->data() + sizeof(IPHeader);
	*((unsigned int*)p) = 1;
	Entry* et = (Entry*)(p + sizeof(unsigned int));
	et->ip = _my_ip;
	et->dis = 0;

	for (int i = 0; i < _link_num; ++i){
		Packet *tmp_packet = header_wrap<PortHeader>(packet);
		(header_get<PortHeader>(tmp_packet))->port = i;
		output(0).push(tmp_packet);
	}
	packet->kill();
}

void ClientIP::run_timer(Timer *timer)
{
	if (timer == &_timer_bocast)
	{
		Broadcast_Table();
		_timer_bocast.reschedule_after_sec(2);
	} else if (timer == &_timer_pull)
	{
		Packet *packet = pull(0);
		while (packet != NULL) {
			packet_classifier(0, packet);
			packet = pull(0);
		}
		_timer_pull.reschedule_after_sec(0.1);
	} else {
		click_chatter("Wrong timer");
		assert(false);
	}
}

void ClientIP::TurnInTransport(Packet *packet_wrap) {
	Packet *packet = header_unpack<IPHeader>(packet_wrap);
	output(1).push(packet);
	packet_wrap->kill();
}

void ClientIP::packet_classifier(int port0, Packet *packet_wrap) {
	assert(packet_wrap);
	assert(port0 == 0);
	
	//int port = header_get<PortHeader>(packet_wrap)->port;
	Packet *packet = header_unpack<PortHeader>(packet_wrap);
	packet_wrap->kill();

	IPHeader *header = header_get<IPHeader>(packet);
	if(header->type == IP_DATA) {
		//click_chatter("Received Data from %u with destination %u", header->source, header->destination);
		if (header->dst_ip == _my_ip) {
			TurnInTransport(packet);
		}
		else {
			click_chatter("Client received a packet with wrong IP\n");
			packet->kill();
		}
	} else if(header->type == IP_ROUNTING) {
		packet->kill();
	} else {
		//click_chatter("Wrong packet type %s\n", packet->data());
		packet->kill();
	}
}

CLICK_ENDDECLS 
EXPORT_ELEMENT(ClientIP)
