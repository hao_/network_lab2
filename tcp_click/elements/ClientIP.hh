#ifndef CLICK_CLIENTIP_HH 
#define CLICK_CLIENTIP_HH 
#include <click/element.hh>
#include <click/timer.hh>
#include <click/hashtable.hh>
#include <click/error.hh>


CLICK_DECLS
	
class ClientIP : public Element {
    public:
        ClientIP();
        ~ClientIP();
        int initialize(ErrorHandler*);
        const char *class_name() const { return "ClientIP";}
        const char *port_count() const { return "2/2";}
        const char *processing() const { return "a/h"; }

        int configure(Vector<String> &conf, ErrorHandler *errh);
        void push(int, Packet*);
        void run_timer(Timer *timer);

    private:
    	uint32_t _my_ip;
    	uint16_t _link_num;
    	String _my_address;
        Timer _timer_bocast, _timer_pull;

        void packet_classifier(int port, Packet *packet_warp);
        void SendToRouter(Packet *packet);
        void Broadcast_Table();
        void TurnInTransport(Packet*);
}; 

CLICK_ENDDECLS
#endif 
