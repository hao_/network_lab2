#include <stdint.h>

typedef enum{
	IP_DATA = 0,
	IP_ROUNTING,
} ip_packe_types;

struct IPHeader{
	uint8_t type;
	uint32_t src_ip;
	uint32_t dst_ip;
	uint32_t packet_size;
};
