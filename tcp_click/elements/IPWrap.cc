#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh>
#include <assert.h>
#include "IPHeader.hh"
#include "IPWrap.hh"
#include "header_helper.cc"
#include "helper.hh"
#include "helper.cc"

CLICK_DECLS

int IPWrap::configure(Vector<String> &conf, ErrorHandler *errh) {
	String srcbuf, dstbuf;
	if (cp_va_kparse(conf, this, errh,
					"SRC", cpkM, cpString, &srcbuf,
					"DST", cpkM, cpString, &dstbuf,
					"TYPE", cpkM, cpUnsigned, &_ip_type,
					cpEnd) < 0) {
		return -1;
	}
	_src_ip = translateIP(srcbuf.c_str());
	_dst_ip = translateIP(dstbuf.c_str());
	
	return 0;
}

void IPWrap::push(int port, Packet *packet) {
	assert(port == 0);
	Packet *wrapper = header_wrap<IPHeader>(packet);
	IPHeader *header = header_get<IPHeader>(wrapper);
	//click_chatter("IP: %x %x\n", _src_ip, _dst_ip );
	header->type = _ip_type;
	header->src_ip = _src_ip;
	header->dst_ip = _dst_ip;
	header->packet_size = wrapper->length();

	output(0).push(wrapper);
	packet->kill();
}

CLICK_ENDDECLS
EXPORT_ELEMENT(IPWrap)
