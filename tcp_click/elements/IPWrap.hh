#ifndef CLICK_IPWRAP_HH
#define CLICK_IPWRAP_HH

#include <click/element.hh>
#include <click/timer.hh>

CLICK_DECLS

class IPWrap : public Element {
	public:
		IPWrap() {};
		~IPWrap() {};
		const char *class_name() const { return "IPWrap"; }
		const char *port_count() const { return "1/1"; }
		const char *processing() const { return "h/h"; }

		void push(int port, Packet *packet);
		int initialize(ErrorHandler *) {return 0;}
		int configure(Vector<String> &conf, ErrorHandler *errh);
	private:
		uint32_t _src_ip, _dst_ip;
		uint8_t _ip_type;
};

CLICK_ENDDECLS
#endif
