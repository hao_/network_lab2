#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh>
#include <assert.h>
#include "IPHeader.hh"
#include "TCPHeader.hh"
#include "IPWrapTCP.hh"
#include "header_helper.cc"
#include "helper.hh"

CLICK_DECLS

void IPWrapTCP::push(int port, Packet *packet) {
	assert(port == 0);
	Packet *wrapper = header_wrap<IPHeader>(packet);

	TCPHeader *inner_header = header_get<TCPHeader>(packet);
	IPHeader *wrapper_header = header_get<IPHeader>(wrapper);

	wrapper_header->type = IP_DATA;
	wrapper_header->src_ip = inner_header->src_ip;
	wrapper_header->dst_ip = inner_header->dst_ip;
	wrapper_header->packet_size = wrapper->length();

	output(0).push(wrapper);
	packet->kill();
}

CLICK_ENDDECLS
EXPORT_ELEMENT(IPWrapTCP)
