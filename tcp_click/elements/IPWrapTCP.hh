#ifndef CLICK_IPWRAPTCP_HH
#define CLICK_IPWRAPTCP_HH

#include <click/element.hh>
#include <click/timer.hh>

CLICK_DECLS

class IPWrapTCP : public Element {
	public:
		IPWrapTCP() {};
		~IPWrapTCP() {};
		const char *class_name() const { return "IPWrapTCP"; }
		const char *port_count() const { return "1/1"; }
		const char *processing() const { return "h/h"; }

		void push(int port, Packet *packet);
	private:
};

CLICK_ENDDECLS
#endif
