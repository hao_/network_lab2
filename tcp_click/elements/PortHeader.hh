#ifndef PORTHEADER_HH
#define PORTHEADER_HH

#include <stdint.h>
#include <click/packet.hh>

struct PortHeader{
	uint16_t port;

	uint16_t port_num(Packet *packet) { 
		return ((struct PortHeader*)packet->data())->port; 
	}
	void* payload(Packet *packet) { 
		return (void*)(packet->data()+sizeof(struct PortHeader)); 
	};
};


#endif
