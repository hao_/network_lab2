#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh>
#include <assert.h>
#include "PortHeader.hh"
#include "PortWrap.hh"
#include "header_helper.cc"

CLICK_DECLS

int PortWrap::configure(Vector<String> &conf, ErrorHandler *errh) {
	if (cp_va_kparse(conf, this, errh,
					"PORT", cpkM, cpUnsigned, &_port,
					cpEnd) < 0) {
		return -1;
	}
	
	return 0;
}

void PortWrap::push(int port, Packet *packet) {
	assert(port == 0);
	Packet *wrapper = header_wrap<PortHeader>(packet);
	PortHeader *header = header_get<PortHeader>(wrapper);
	//click_chatter("IP: %x %x\n", _src_ip, _dst_ip );
	header->port = _port;

	output(0).push(wrapper);
	packet->kill();
}

CLICK_ENDDECLS
EXPORT_ELEMENT(PortWrap)
