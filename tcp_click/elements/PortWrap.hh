#ifndef CLICK_PORTWRAP_HH
#define CLICK_PORTWRAP_HH

#include <click/element.hh>
#include <click/timer.hh>

CLICK_DECLS

class PortWrap : public Element {
	public:
		PortWrap() {};
		~PortWrap() {};
		const char *class_name() const { return "PortWrap"; }
		const char *port_count() const { return "1/1"; }
		const char *processing() const { return "h/h"; }

		void push(int port, Packet *packet);
		int initialize(ErrorHandler *) {return 0;}
		int configure(Vector<String> &conf, ErrorHandler *errh);
	private:
		uint16_t _port;
};

CLICK_ENDDECLS
#endif
