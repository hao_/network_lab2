#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh> 
#include <click/timer.hh>
#include "RouterIP.hh"
#include "IPHeader.hh"
#include "PortHeader.hh"
#include "header_helper.cc"
#include "DVPacket.hh"

#include "helper.hh"

CLICK_DECLS 

RouterIP::RouterIP() : _timer_bocast(this), _timer_pull(this) {
}

RouterIP::~RouterIP(){	
}

int RouterIP::initialize(ErrorHandler*){
    _my_ip = 0;
    const char* str = _my_address.c_str();
    unsigned int tmp = 0, len = strlen(str);
    for (unsigned int i = 0; i < len; ++i)
    	if (str[i] == '.')
    	{
    		_my_ip <<= 8;
    		_my_ip += tmp;
    		tmp = 0;
    	} else tmp = 10 * tmp + str[i] - '0'; 
    _my_ip <<= 8;
    _my_ip += tmp;
    _dis_table.set(_my_ip, 0);
    _ports_table.set(_my_ip, -1);
    _change = true;
    _timer_bocast.initialize(this);
    _timer_bocast.schedule_now();
    _timer_pull.initialize(this);
    _timer_pull.schedule_now();
    return 0;
}


void RouterIP::Forwarding(unsigned int dst_ip, Packet *packet){
	if (_ports_table.find(dst_ip))
	{
		int next_port = _ports_table.get(dst_ip);
		Packet* packet_wrap = header_wrap<PortHeader>(packet);
		header_get<PortHeader>(packet_wrap)->port = next_port;
		packet->kill();
		
		output(0).push(packet_wrap);
	} else packet->kill();
}

int RouterIP::configure(Vector<String> &conf, ErrorHandler *errh) {
	if (cp_va_kparse(conf, this, errh,
				  "MY_ADDRESS", cpkM, cpString, &_my_address,
				  "LINK_NUM", cpkM, cpUnsigned, &_link_num,
				  cpEnd) < 0) {
	return -1;
	}
	//click_chatter("configure : %s %u ", _my_address.c_str(), _link_num);
	return 0;
}

void RouterIP::Broadcast_Table()
{
	/*
	WritablePacket *packet1 = Packet::make(0,0,2, 0);
	Packet *tmp_packet1 = header_wrap<PortHeader>(packet1);
	header_get<PortHeader>(tmp_packet1)->port = 0;
	output(0).push(tmp_packet1);
	return;*/
	WritablePacket *packet = Packet::make(40,0,sizeof(IPHeader) 
					+ sizeof(unsigned int) + sizeof(Entry) * _dis_table.size(), 28);

	memset(packet->data(),0,packet->length());

	IPHeader *format = (IPHeader*) packet->data();
	format->src_ip = _my_ip;
	format->dst_ip = -1;
	format->type = IP_ROUNTING;
	format->packet_size = packet->length();
	
	char *p = (char*)packet->data() + sizeof(IPHeader);
	*((unsigned int*)p) = _dis_table.size();
	Entry* et = (Entry*)(p + sizeof(unsigned int));
	//click_chatter("Routering table : ");
	//print_ip_dot(_my_ip);
	for (HashTable<unsigned int, unsigned int>::iterator it = _dis_table.begin(), en = _dis_table.end(); it != en; ++it)
	{
		et->ip = it.key();
		et->dis = it.value();
		//print_ip_dot(et->ip);
		//click_chatter(" %u\n",  et->dis);
		++et;
	}
	for (int i = 0; i < _link_num; ++i){
		Packet *tmp_packet = header_wrap<PortHeader>(packet);
		(header_get<PortHeader>(tmp_packet))->port = i;
		output(0).push(tmp_packet);
	}
	packet->kill();
}

void RouterIP::run_timer(Timer *timer)
{
	if (timer == &_timer_bocast)
	{
		if (_change)
		{
			Broadcast_Table();
		}
		_change = false;
		_timer_bocast.reschedule_after_sec(0.1);
	} else if (timer == &_timer_pull)
	{
		Packet *packet = pull(0);
		while (packet != NULL) {
			packet_classifier(0, packet);
			packet = pull(0);
		}
		_timer_pull.reschedule_after_sec(0.1);
	} else {

		click_chatter("Wrong timer");
		assert(false);
	}
}

void RouterIP::Routering(int port, Packet *packet){
	char *p = header_payload<IPHeader>(packet);
	unsigned int size = *(unsigned int*)p;
	p += sizeof(unsigned int);

	unsigned int ip, dis;
	for (unsigned int i = 0; i < size; ++i)
	{
		Entry *et = (Entry*)p;
		p += sizeof(Entry);

		ip = et->ip;
		dis = et->dis;
		if (ip == _my_ip) continue;

		if (!_dis_table.find(ip))
		{
			_ports_table.set(ip, port);
			_dis_table.set(ip, dis + 1);
			_change = true;
		} else if (dis + 1 < _dis_table.get(ip))
		{
			_ports_table.set(ip, port);
			_dis_table.set(ip, dis + 1);
			_change = true;
		}
	}

}

void RouterIP::packet_classifier(int port0, Packet *packet_wrap) {
	assert(packet_wrap);
	//click_chatter("port0 %d", port0);
	assert(port0 == 0);
	int port = header_get<PortHeader>(packet_wrap)->port;
	Packet *packet = header_unpack<PortHeader>(packet_wrap);
	packet_wrap->kill();

	IPHeader *header = header_get<IPHeader>(packet);
	if(header->type == IP_DATA) {
		//click_chatter("Received Data from %u with destination %u", header->source, header->destination);
		if (header->dst_ip == _my_ip)
			packet->kill();
		Forwarding(header->dst_ip, packet);
	} else if(header->type == IP_ROUNTING) {
		//click_chatter("Received Hello from %u on port %d", header->source, port);
		Routering(port, packet);
		packet->kill();
		//_ports_table.set(header->src_ip, port);
	} else {
		//click_chatter("Wrong packet type %s\n", packet->data());
		packet->kill();
	}
}

CLICK_ENDDECLS 
EXPORT_ELEMENT(RouterIP)
