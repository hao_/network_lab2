#ifndef CLICK_ROUTERIP_HH 
#define CLICK_ROUTERIP_HH 
#include <click/element.hh>
#include <click/timer.hh>
#include <click/hashtable.hh>
#include <click/error.hh>


CLICK_DECLS
	
class RouterIP : public Element {
    public:
        RouterIP();
        ~RouterIP();

        const char *class_name() const { return "RouterIP";}
        const char *port_count() const { return "1/1";}
        const char *processing() const { return "l/h"; }

        int initialize(ErrorHandler*);
        int configure(Vector<String> &conf, ErrorHandler *errh);
        void run_timer(Timer *timer);

    private:
    	uint32_t _my_ip;
    	uint16_t _link_num;
    	String _my_address;
    	bool _change;	
        Timer _timer_bocast, _timer_pull;
    	HashTable<unsigned int, int> _ports_table;
    	HashTable<unsigned int, unsigned int> _dis_table;

        void Forwarding(unsigned int dst_ip, Packet *packet);
        void Routering(int port, Packet *packet);
        void Broadcast_Table();
        void packet_classifier(int port, Packet *packet_warp);
}; 

CLICK_ENDDECLS
#endif 
