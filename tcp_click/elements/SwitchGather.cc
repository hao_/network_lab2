#include <click/config.h>
#include <click/packet.hh>
#include "PortHeader.hh"
#include "SwitchGather.hh"
#include "header_helper.cc"

CLICK_DECLS

void SwitchGather::push(int port, Packet *packet) {
	Packet *wrapper = header_wrap<PortHeader>(packet);
	PortHeader *header = header_get<PortHeader>(wrapper);
	header->port = port;
	output(0).push(wrapper);
	packet->kill();
}

CLICK_ENDDECLS
EXPORT_ELEMENT(SwitchGather)
