#ifndef CLICK_SWITCHGATHER_HH
#define CLICK_SWITCHGATHER_HH

#include <click/element.hh>
#include <click/timer.hh>

CLICK_DECLS

class SwitchGather : public Element {
	public:
		SwitchGather() {};
		~SwitchGather() {};
		const char *class_name() const { return "SwitchGather"; }
		const char *port_count() const { return "-/1"; }
		const char *processing() const { return "h/h"; }

		void push(int port, Packet *packet);
		int initialize(ErrorHandler *) {return 0;}
};

CLICK_ENDDECLS
#endif
