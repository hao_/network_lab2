#include <click/config.h>
#include <click/packet.hh>
#include <assert.h>
#include "PortHeader.hh"
#include "SwitchSplit.hh"
#include "header_helper.cc"

CLICK_DECLS

void SwitchSplit::push(int port, Packet *packet) {
	assert(port == 0);
	Packet *inner = header_unpack<PortHeader>(packet);
	PortHeader *header = header_get<PortHeader>(packet);
	
	int out_port = header->port;
	output(out_port).push(inner);
	packet->kill();
}

CLICK_ENDDECLS
EXPORT_ELEMENT(SwitchSplit)
