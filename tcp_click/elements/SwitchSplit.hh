#ifndef CLICK_SWITCHSPLIT_HH
#define CLICK_SWITCHSPLIT_HH

#include <click/element.hh>
#include <click/timer.hh>

CLICK_DECLS

class SwitchSplit : public Element {
	public:
		SwitchSplit() {};
		~SwitchSplit() {};
		const char *class_name() const { return "SwitchSplit"; }
		const char *port_count() const { return "1/-"; }
		const char *processing() const { return "h/h"; }

		void push(int port, Packet *packet);

	private:
};

CLICK_ENDDECLS
#endif
