#ifndef CLICK_TCPHEADER_HH
#define CLICK_TCPHEADER_HH

#include <stdint.h>

typedef enum{
	TCP_DATA = 0,
	TCP_DATAACK,
	TCP_CONNECT,
	TCP_LISTEN,
	TCP_CLOSE,
	TCP_SYN,
	TCP_SYNACK,
	TCP_ACK,
	TCP_FIN,
	TCP_FINACK,
	TCP_ASK_TO_CLOSE
} tcp_packe_types;

struct TCPHeader{
	uint8_t type;
	uint32_t src_ip;
	uint16_t src_port;
	uint32_t dst_ip;
	uint16_t dst_port;
	uint32_t packet_size;
	uint32_t rest_win; // For flow control
	uint32_t seq, ackseq;
};

#endif
