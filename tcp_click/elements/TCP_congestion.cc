#include <math.h>
#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh> 
#include <click/timer.hh>
#include <click/config.h>
#include "TCP_congestion.hh"
#include "header_helper.cc"
#include "TCPHeader.hh"

CLICK_DECLS

static Packet* PacketCopy(Packet *packet) {
	WritablePacket *newp = Packet::make(40, 0, packet->length(), 28);
	memcpy(newp->data(), packet->data(), packet->length());

	return newp;
}

static uint32_t next_seq(uint32_t seq, uint32_t num = 1) //calculate next sequence number
{
	return (seq + num) % MAX_QUEUE_SIZE;
}

static bool is_before(uint32_t seq1, uint32_t seq2) //return whether seq1 is before seq2
{
	if (seq1 < seq2)
	{
		if (seq1 + MAX_QUEUE_SIZE / 2 > seq2) return true;
			else return false;
	} else {
		if (seq2 + MAX_QUEUE_SIZE / 2 > seq1) return false;
			else return true;
	}
}

static bool is_after(uint32_t seq1, uint32_t seq2) //return whether seq1 is after seq2
{
	if (seq1 != seq2 && !is_before(seq1, seq2)) return true;
		else return false;
}


template<class Pack>
Pack*& CC_queue<Pack>::operator[] (int seq)
{
	return pack[seq];
}
template<class Pack>
void CC_queue<Pack>::insert(Pack* packet)
{
	data_tail = next_seq(data_tail);
	pack[data_tail] = packet;
}
template<class Pack>
bool CC_queue<Pack>::empty()
{
	return LAR == data_tail;
}
template<class Pack>
Pack*& CC_queue<Pack>::front()
{
	return pack[next_seq(LAR)];
	//if (empty()) return NULL;
	//	else return pack[next_seq(LAR)];
}
template<class Pack>
void CC_queue<Pack>::pop()
{
	LAR = next_seq(LAR);
}
template<class Pack>
void CC_queue<Pack>::clean()
{
	LAR = LFS = data_tail = 0;
}

//creat a TCP ACK packet
Packet* Port_pack_processor::create_ACK(uint32_t ack_seq, uint32_t rest_win, TCPHeader* packet_header)
{
	WritablePacket *packet = Packet::make(28, 0, sizeof(TCPHeader), 40);
	TCPHeader *header = header_get<TCPHeader>(packet);
	header->ackseq = ack_seq;
	header->rest_win = rest_win;
	header->type = TCP_DATAACK;
	header->src_ip = packet_header->dst_ip;
	header->dst_ip = packet_header->src_ip;
	header->src_port = packet_header->dst_port;
	header->dst_port = packet_header->src_port;
	return packet;
}

//send packet to IP layer
void Port_pack_processor::send_packet()
{
	if (!send_queue.empty())
	{
		bool is_resend = false;
		int i = next_seq(send_queue.LAR);
		while (!is_after(i, send_queue.LFS))
		{
			if (send_queue[i]->to_retransmit) {
				is_resend = true;
				break; //find the packet need retransmit  
			}
			i = next_seq(i);
		}
		//if (!is_resend)
		///	i = next_seq(send_queue.LAR);
		if (!is_after(i, send_queue.data_tail) && !is_after(i, next_seq(send_queue.LAR, send_queue_win_size()))) 
		{
			CC_sender_packet *packet = send_queue[i];

			
			packet->timer->clear();
			packet->timer->schedule_after_sec(time_out);
			packet->ack_time = 0;
			packet->to_retransmit = false;

			
			if (is_resend) {
				click_chatter("Retransmit loss packet %d, (cwnd %.2f)", i, cwnd);
			}
			else
				click_chatter("Send new packet %d, (cwnd %.2f)", i, cwnd);
			//click_chatter("LAR %d LFS %d data_tail %d i %d\n", send_queue.LAR, send_queue.LFS, send_queue.data_tail, i);
			assert(header_get<TCPHeader>(packet->packet)->type == TCP_DATA);
			
			elem->send_to_ip( PacketCopy(packet->packet) );
			//insert_send_queue(1, packet->packet);
			if (is_after(i, send_queue.LFS))
				send_queue.LFS = i;
		}
	}
	timer->reschedule_after_sec(RTT / cwnd);
}


//receive a TCP_DATA packet from IP layer
void Port_pack_processor::recv_packet(CC_receiver_packet* packet)
{
	TCPHeader *header = header_get<TCPHeader>(packet);
	uint32_t seq = header->seq;

	if (is_after(seq, next_seq(receive_queue.LAR, receive_queue_win_size())) 
		|| is_before(seq, next_seq(receive_queue.LAR)))
	{
		packet->kill();
	} else if (seq == next_seq(receive_queue.LAR))
	{
		receive_queue[seq] = packet;
		while (receive_queue.front() != NULL)
		{
			//insert_send_queue(0, receive_queue.front()); //set to APP layer
			elem->send_to_app( receive_queue.front() );

			receive_queue.front() = NULL;
			receive_queue.pop();
			//receive_queue[next_seq(receive_queue.LAR)] = NULL;
		}
		if (is_after(seq, receive_queue.LFS))
			receive_queue.LFS = seq;
	}  else {
		if (is_after(seq, receive_queue.LFS))
			receive_queue.data_tail = receive_queue.LFS = seq;
		receive_queue[seq] = packet;
	}
	//send an ACK back
	click_chatter("send an ack for seq %d", seq);
	elem->send_to_ip( create_ACK(receive_queue.LAR, 
					receive_queue_win_size() - receive_queue.LFS + receive_queue.LAR, header));
}


//set _to_retransmit label to true
void Port_pack_processor::retransmit(CC_sender_packet* packet_buff)
{
	assert(packet_buff->packet != NULL);
	packet_buff->timer->clear();
	packet_buff->to_retransmit = true; 
	//cwnd *= 0.7; // multiplicative decrease
	cwnd *= exp(log(0.5) / cwnd); 
	if (cwnd < 1) cwnd = 1;
}

//receive an ACK packet
void Port_pack_processor::recv_ack(CC_receiver_packet* packet_ack)
{
	TCPHeader *header = header_get<TCPHeader>(packet_ack);
	uint32_t ackseq = header->ackseq, rest_win = header->rest_win;

	if (cwnd > rest_win) //flow control
	{
		cwnd = rest_win;
		if (rest_win == 0)
		{
			cwnd = 2;
			slow_start = true;
		}
	} else {
		if (slow_start) cwnd += 1;  //slow-start
			else cwnd += 1 / cwnd;  //additive increase
		if (cwnd + 1 > MAX_QUEUE_SIZE)
			cwnd = MAX_QUEUE_SIZE - 1;
	}
	if (is_after(ackseq, send_queue.LAR)) //packets in [send_queue.LAR + 1, ackseq] have arrived
	{
		Packet *last_packet = NULL;
		do{
			if (last_packet) {
				last_packet->kill();
			}
			assert(send_queue.front());
			last_packet = send_queue.front()->packet;
			send_queue.front()->packet = NULL;

			send_queue.front()->timer->clear();
			send_queue.pop();
		} while (send_queue.LAR != ackseq);

		if (send_queue.empty() && ready_to_close) {
			elem->close(last_packet);
		}	
	} else if (ackseq == send_queue.LAR) 
	{
		//fast retransmittion
		if (++send_queue[ackseq]->ack_time > 3 && send_queue[next_seq(ackseq)]->packet != NULL) 
		{
			send_queue[ackseq]->ack_time = 0;
			click_chatter("Detect 3 duplicate ACK, fast retransmission, MD");
			retransmit(send_queue[next_seq(ackseq)]);
			slow_start = false;
		}
	}
}


//put a new packet from app layer into the send_queue
void Port_pack_processor::packet_from_app(Packet *packet)
{
	//pay attention
	int next_tail = next_seq(send_queue.data_tail);
	header_get<TCPHeader>(packet)->seq = next_tail;
	CC_sender_packet *packet_send = send_queue[next_tail];
	packet_send->packet = packet;
	packet_send->to_retransmit = false;
	assert( header_get<TCPHeader>(packet_send->packet)->type == TCP_DATA );
	send_queue.insert(packet_send);
}

void TCP_congestion::send_to_app(Packet *packet) {
	output(0).push(packet);
}
void TCP_congestion::send_to_ip(Packet *packet) {
	//TCPHeader *header = header_get<TCPHeader>(packet);
	output(1).push(packet);
}

TCP_congestion::TCP_congestion(){	
}

TCP_congestion::~TCP_congestion(){	
}

int TCP_congestion::configure(Vector<String> &conf, ErrorHandler *errh) {
  if (cp_va_kparse(conf, this, errh,
                  "RATE", cpkM, cpUnsigned, &_rate,
                  cpEnd) < 0) {
    return -1;
  }
  for (int i = 0; i < MAXPORT; ++i)
	_port[i].connect = false;
  return 0;
}

int TCP_congestion::initialize(ErrorHandler*){
    return 0;
}


//handle with a connect TCP packet
void TCP_congestion::connect(uint32_t port_no)
{
	if (_port[port_no].connect) return;
	_port[port_no].connect = true;
	_port[port_no].timer = new Timer(this);
	_port[port_no].timer->initialize(this);
	_port[port_no].RTT = (double)2000 / _rate;
	//_port[port_no].time_out = 2 * _port[port_no].RTT;
	_port[port_no].time_out = 3;
	_port[port_no].seq = 0;
	_port[port_no].cwnd = 10;
	_port[port_no].slow_start = true;
	_port[port_no].elem = this;

	_timer_port.set(_port[port_no].timer, port_no);
	for (int i = 0; i < MAX_QUEUE_SIZE; ++i)
	{
		_port[port_no].send_queue[i] = new CC_sender_packet();

		_port[port_no].send_queue[i]->timer = new Timer(this);
		_port[port_no].send_queue[i]->timer->initialize(this);
		_port[port_no].send_queue[i]->port_no = port_no;
		_timer_finder.set(_port[port_no].send_queue[i]->timer, _port[port_no].send_queue[i]);
	}
	_port[port_no].send_queue.clean();
	_port[port_no].receive_queue.clean();
	_port[port_no].timer->schedule_after_sec(
			_port[port_no].RTT / _port[port_no].cwnd);
}


//handle with a close TCP packet
void TCP_congestion::close(Packet *packet)
{
	TCPHeader *header = header_get<TCPHeader>(packet);
	uint16_t port_no = header->src_port;
	Port_pack_processor &pp =  _port[port_no];
	if (!pp.connect) return;

	click_chatter("Finish transmission on port %d, close it", port_no);

	header->type = TCP_CLOSE;
	assert(pp.send_queue.empty());
	output(0).push(packet);
	
	pp.connect = false;
	pp.timer->clear();
	for (int i = 0; i < MAX_QUEUE_SIZE; ++i)
	{
		assert(pp.send_queue[i] != NULL);

		pp.send_queue[i]->timer->clear();

		if (pp.send_queue[i]->packet) {
			assert(!is_after(i, pp.send_queue.data_tail));
			if (is_after(i, pp.send_queue.LFS)) {
				pp.send_queue[i]->packet->kill();
				pp.send_queue[i]->packet = NULL;
			}
		}
		if (pp.receive_queue[i]) {
			pp.receive_queue[i]->kill();
			pp.receive_queue[i] = NULL;
		}
	}
}


void TCP_congestion::run_timer(Timer *timer) {
	if (_timer_port.find(timer))
	{
		uint32_t port_no = _timer_port.get(timer);
		_port[port_no].send_packet();
	} else {
		assert(_timer_finder.find(timer));
		CC_sender_packet *packet_buff = _timer_finder.get(timer);
		uint32_t port_no = packet_buff->port_no;
		click_chatter("Detect a TIMEOUT loss, MD");
		_port[port_no].retransmit(packet_buff);
	}
}

void TCP_congestion::push(int port_click, Packet *packet)
{
	TCPHeader *header = header_get<TCPHeader>(packet);
	uint8_t type = header->type;

	if (port_click == 1) //from IP layer
	{
		uint32_t port_no = header->dst_port;
		if (!_port[port_no].connect) {
			packet->kill();
			return;
		}
		if (type == TCP_DATA)
		{
			_port[port_no].recv_packet(packet);
		} else {
			assert(type = TCP_DATAACK);
			_port[port_no].recv_ack(packet);
		}
	} else if (port_click == 0) { //from APP layer
		uint32_t port_no = header->src_port;
		if (type == TCP_CONNECT)
		{
			connect(port_no);
		} else if (type == TCP_CLOSE){
			_port[port_no].ready_to_close = true;
			if (_port[port_no].send_queue.empty())
				close(packet);
			else
				packet->kill();
		} else {
			assert(type == TCP_DATA);
			if (!_port[port_no].connect) { 
				//packet->kill();
				click_chatter("Get TCP_DATA packet when not connected");
				assert(false);
			}
			_port[port_no].packet_from_app(packet);
		}
	} else {
		click_chatter("Packet from wrong port %d", port_click);
		assert(false);
	}
}

CLICK_ENDDECLS
EXPORT_ELEMENT(TCP_congestion)
