#ifndef CLICK_TCP_CONGESTION_HH
#define CLICK_TCP_CONGESTION_HH

#include <click/config.h>
#include <click/packet.hh> 
#include <click/timer.hh>
#include <algorithm>
#include <click/hashtable.hh>
#include "TCPHeader.hh"
#include "CCstruct.hh"

const int MAXPORT = 256;

CLICK_DECLS

class TCP_congestion : public Element {
	public:
		TCP_congestion();
		~TCP_congestion();
		const char* class_name() const { return "TCP_congestion"; }
		const char* port_count() const { return "2/2"; }
		const char* processing() const { return "h/h"; }
		
		void push(int port_click, Packet *packet_wrap);
		int initialize(ErrorHandler*);
		void run_timer(Timer *timer);
		void connect(uint32_t port_no);
		void close(Packet*);
		int configure(Vector<String> &conf, ErrorHandler *errh);
		/*static CC_sender_packet send_queue[100];
		static uint32_t queue_size;*/

		void send_to_app(Packet*);
		void send_to_ip(Packet*);
	private:
		HashTable<Timer*, CC_sender_packet*> _timer_finder;
		HashTable<Timer*, uint32_t> _timer_port;
		Port_pack_processor _port[MAXPORT];
		int _rate;
};

CLICK_ENDDECLS
#endif
