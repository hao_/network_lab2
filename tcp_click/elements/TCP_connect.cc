#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh> 
#include <click/timer.hh>
#include "connection.hh"
#include "TCPHeader.hh"
#include "TCP_connect.hh"
#include "header_helper.cc"

CLICK_DECLS

#define TCP_CONGESTION

/*
static Packet* new_packet() {
	return Packet::make(40,0, sizeof(TCPHeader), 28);
}*/

static Packet* PacketCopy(Packet* packet) {
	WritablePacket *newp = Packet::make(40, 0, packet->length(), 28);
	memcpy(newp->data(), packet->data(), packet->length());
	
	return newp;
}

static Packet* swap_src_dst(Packet *packet) {
	TCPHeader* header = header_get<TCPHeader>(packet);
	uint32_t tmp = header->dst_port;
	header->dst_port = header->src_port;
	header->src_port = tmp;

	tmp = header->dst_ip;
	header->dst_ip = header->src_ip;
	header->src_ip = tmp;
	return packet;
}

static uint32_t rand_seq() {
	return rand()*rand() % 100000;
}


void TCP_connect::push(int port, Packet *packet) {

	TCPHeader *header = header_get<TCPHeader>(packet);
	uint16_t type = header->type;
	uint16_t src_port = header->src_port;

	// Another APP use to click port
	if (port > 2 && port_to_click_port[src_port] != port) {
		port_to_click_port[src_port] = port;
	}

#ifdef	TCP_CONGESTION
	if (header->type == TCP_DATA || header->type == TCP_DATAACK) {
		// DATA or DATACK packet
		if (port == 0) {
			// packet from IP
			uint8_t status = get_status(header->dst_port);
			if (status == PORT_CONNECT || status == PORT_SYN_RCVD) {
				// Connected, forward to TCP_congestion
				output(1).push(packet);
			}
			else {
				// Not connected, drop it
				packet->kill();
			}
		} else if (port == 1) {
			// packet from TCP_congestion to send out
			output(0).push(packet);
		} else if (port == 2) {
			// APP packet output
			assert(port_to_click_port[header->dst_port] != -1);
			output(port_to_click_port[header->dst_port]).push(packet);
		} else {
			// packet from APP, forward to TCP_congestion
			output(2).push(packet);
		}
	} else {
		// Packet used to establish connection
		if (port == 0) {
			// From IP
			Resender_in(packet);
		} else if (port > 2) {
			// From APP
			if (type == TCP_CLOSE) {
				output(2).push(packet);
			} else {
				DFA_From_APP(packet);
			}
		} else {
			if (type == TCP_CLOSE) {
				DFA_From_APP( packet );
			} else {
				click_chatter("Receive a non-data packet from TCP_congestion");
				assert(false);
			}
		}
	}
#else

	if (header->type == TCP_DATA || header->type == TCP_DATAACK) {
		// DATA or DATACK packet
		if (port == 0) {
			// packet from IP
			uint8_t status = get_status(header->dst_port);
			if (status == PORT_CONNECT || status == PORT_SYN_RCVD) {
				// Connected, send to APP
				assert(port_to_click_port[header->dst_port] != -1);
				output(port_to_click_port[header->dst_port]).push(packet);
			}
			else {
				// Not connected, drop it
				packet->kill();
			}
		} else if (port == 1) {
			click_chatter("Unexpected packet from tcp_congestion");
		} else if (port == 2) {
			// APP packet output
			click_chatter("Unexpected packet from tcp_congestion");
		} else {
			// Data packet from APP, send out
			output(0).push(packet);
		}
	} else {
		// Packet used to establish connection
		if (port == 0) {
			// From IP
			Resender_in(packet);
		} else if (port > 2) {
			// From APP
			DFA_From_APP(packet);
		} else {
			click_chatter("Unexpected packet from tcp_congestion");
			assert(false);
		}
	}
#endif
}

/* Important note
 * It's weird that initialize was called just after push() was called for several times.
 *
 * So is it the BUG of initialize?
 * */

int TCP_connect::configure(Vector<String>&, ErrorHandler*) {
	//click_chatter("configure");
	for (int i = 0; i < MAX_PORT; i++) {
		_status_table[i].status = PORT_CLOSED;
		_status_table[i].wait_seq = 0;
		_status_table[i].packet_buf = NULL;
		_status_table[i].timer = new Timer(this);
		_status_table[i].timer->initialize(this);
		_timer_finder[_status_table[i].timer] = i;

		port_to_click_port[i] = -1;
	}
	return 0;
}

int TCP_connect::initialize(ErrorHandler*) {
	//click_chatter("init");
	return 0;
}

void TCP_connect::change_status(uint16_t src_port, uint32_t dst_ip, uint32_t dst_port, uint8_t new_status)
{
	port_entry &p = _status_table[src_port];
	p.status = new_status;
	p.dst_ip = dst_ip;
	p.dst_port = dst_port;
}

uint8_t TCP_connect::get_status(uint16_t src_port)
{
	return _status_table[src_port].status;
}

void TCP_connect::send_to_ip(uint8_t type, Packet *packet, uint32_t seq = 0, uint32_t ackseq = 0)
{
	assert(packet);
	TCPHeader* header = header_get<TCPHeader>(packet);
	header->type = type;
	header->seq = seq;
	header->ackseq = ackseq;
	Resender_out(packet);
}


// Control packet that send to app
void TCP_connect::send_to_app(uint8_t type, Packet *packet) {
	TCPHeader* header = header_get<TCPHeader>(packet);
	uint16_t port = header->dst_port;
	header->type = type;

#ifdef TCP_CONGESTION
	if (type != TCP_ASK_TO_CLOSE) {
		// TCP_CONNECT or TCP_CLOSE, also send to TCP_congestion to mark it
		output(2).push( swap_src_dst(packet) );
	}
#endif

	assert(port_to_click_port[port] != -1);
	output(port_to_click_port[port]).push( swap_src_dst(packet) );
}


// DFA state transfer from APP action
void TCP_connect::DFA_From_APP(Packet* packet)
{
	TCPHeader* header = header_get<TCPHeader>(packet);
	uint8_t type = header->type;
	//uint32_t src_ip = header->src_ip;
	uint32_t dst_ip = header->dst_ip;
	uint16_t src_port = header->src_port;
	uint16_t dst_port = header->dst_port;
	uint8_t status = get_status(src_port);
	
	//click_chatter("DFA_From_APP: srcport%d status%d headertype%d", src_port, status, type);


	if (type == TCP_LISTEN)
	{
		if (status != PORT_CLOSED) {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
			
		} else {
			change_status(src_port, dst_ip, dst_port, PORT_LISTEN);
		}
	} else if (type == TCP_CONNECT)
	{
		if (status != PORT_CLOSED) {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		} else {
			change_status(src_port, dst_ip, dst_port, PORT_SYN_SENT);
			send_to_ip(TCP_SYN, packet, rand_seq());
		}
	} else if (type == TCP_CLOSE)
	{
		if (status == PORT_LISTEN || status == PORT_SYN_SENT) {
			change_status(src_port, dst_ip, dst_port, PORT_CLOSED);
			send_to_app(TCP_CLOSE, swap_src_dst(packet));
		} else if (status == PORT_SYN_RCVD){
			send_to_ip(TCP_FIN, packet, rand_seq());
			change_status(src_port, dst_ip, dst_port, PORT_FIN1);
		} else if (status == PORT_CLOSE_WAIT){
			send_to_ip(TCP_FIN, packet, rand_seq());
			change_status(src_port, dst_ip, dst_port, PORT_LAST_ACK);
		} else if (status == PORT_CONNECT){
			send_to_ip(TCP_FIN, packet, rand_seq());
			change_status(src_port, dst_ip, dst_port, PORT_FIN1);
		} else {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		}
	} 
}

// DFA state transfer from IP action
void TCP_connect::DFA_From_IP(Packet* packet)
{
	TCPHeader *header = header_get<TCPHeader>(packet);	
	uint8_t type = header->type;
	uint32_t /*src_ip = header->dst_ip,*/ dst_ip = header->src_ip;
	uint16_t src_port = header->dst_port, dst_port = header->src_port;
	uint8_t status = get_status(src_port);
	uint32_t seq = header->seq;

	//click_chatter("DFA_From_IP: srcport%d status%d headertype%d", src_port, status, type);

	if (status == PORT_CLOSED)
	{
	} else if (status == PORT_LISTEN)
	{
		if (type == TCP_SYN)
		{
			Packet *copied_packet = PacketCopy(packet);
			send_to_ip(TCP_SYNACK, swap_src_dst(packet), rand_seq(), seq + 1);
			change_status(src_port, dst_ip, dst_port, PORT_SYN_RCVD);

			// Tell TCP_congestion that dataIO starts
			TCPHeader *header = header_get<TCPHeader>(copied_packet);
			header->type = TCP_CONNECT;
			output(2).push( swap_src_dst(copied_packet) );

		} else {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		}
	} else if (status == PORT_SYN_RCVD)
	{
		if (type == TCP_ACK){
			change_status(src_port, dst_ip, dst_port, PORT_CONNECT);
			send_to_app(TCP_CONNECT, packet);
		} else if (type == TCP_FIN){
			/*
			set wait_seq packet_buf to 0
			send_to_ip(TCP_ACK, swap_src_dst(packet), 0, seq + 1);
			send_to_app(TCP_ASK_TO_CLOSE, new_packet());
			change_status(src_port, dst_ip, dst_port, PORT_CLOSE_WAIT);*/
		} else {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		}
	} else if (status == PORT_SYN_SENT)
	{
		if (type == TCP_SYN){
			send_to_ip(TCP_SYNACK, swap_src_dst(packet), rand_seq(), seq + 1);
			change_status(src_port, dst_ip, dst_port, PORT_SYN_RCVD);
		} else if (type == TCP_SYNACK){
			Packet *copied_packet = PacketCopy(packet);
			send_to_ip(TCP_ACK, swap_src_dst(packet), 0, seq + 1);
			change_status(src_port, dst_ip, dst_port, PORT_CONNECT);
			send_to_app(TCP_CONNECT, copied_packet);
		} else {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		}
	} else if (status == PORT_CONNECT)
	{
		if (type == TCP_FIN){
			Packet *copied_packet = PacketCopy(packet);
			send_to_ip(TCP_ACK, swap_src_dst(packet), 0, seq + 1);
			send_to_app(TCP_ASK_TO_CLOSE, copied_packet);
			change_status(src_port, dst_ip, dst_port, PORT_CLOSE_WAIT);
		} else if (type == TCP_SYNACK) {
			send_to_ip(TCP_ACK, swap_src_dst(packet), 0, seq + 1);
		} else {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		}

	} else if (status == PORT_FIN1)
	{
		if (type == TCP_FIN){
			send_to_ip(TCP_ACK, swap_src_dst(packet), 0, seq + 1);
			change_status(src_port, dst_ip, dst_port, PORT_CLOSING);
		} else if (type == TCP_FINACK){
			Packet *copied_packet = PacketCopy(packet);
			send_to_ip(TCP_ACK, swap_src_dst(packet), 0, seq + 1);
			change_status(src_port, dst_ip, dst_port, PORT_TIME_WAIT);
			time_wait(src_port, copied_packet);
		} else if (type == TCP_ACK){
			packet->kill();
			change_status(src_port, dst_ip, dst_port, PORT_FIN2);
		} else {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		}
	} else if (status == PORT_FIN2)
	{
		if (type == TCP_FIN){
			Packet *copied_packet = PacketCopy(packet);
			send_to_ip(TCP_ACK, swap_src_dst(packet), 0, seq + 1);
			change_status(src_port, dst_ip, dst_port, PORT_TIME_WAIT);
			time_wait(src_port, copied_packet);
		} else {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		}
	} else if (status == PORT_CLOSING)
	{
		if (type == TCP_ACK){
			change_status(src_port, dst_ip, dst_port, PORT_TIME_WAIT);
			time_wait(src_port, packet);
		} else {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		}
	} else if (status == PORT_CLOSE_WAIT)
	{
		#ifdef WRONGTCPTYPE
		click_chatter("wrong tcp packet received");
		return;
		#endif
	} else if (status == PORT_LAST_ACK)
	{
		if (type == TCP_ACK){
			change_status(src_port, dst_ip, dst_port, PORT_CLOSED);
			send_to_app(TCP_CLOSE, packet);
		} else {
			#ifdef WRONGTCPTYPE
			click_chatter("wrong tcp packet received");
			return;
			#endif
		}
	} else if (status == PORT_TIME_WAIT) {
		send_to_ip(TCP_ACK, swap_src_dst(packet), 0, seq + 1);
		_status_table[src_port].timer->clear();
		_status_table[src_port].timer->schedule_after_sec(_timeout * 2);
	}
}

// Special Case: Timewait state, to schedule closing
void TCP_connect::time_wait(int port, Packet *packet) {
	port_entry &p_entry = _status_table[port];

	assert(p_entry.wait_seq == 0 && p_entry.packet_buf == NULL);
	
	p_entry.wait_seq = 1;
	p_entry.packet_buf = packet;
	p_entry.timeout_times = 0;
	p_entry.timer->clear();
	p_entry.timer->schedule_after_sec(_timeout * 2);
}

// Process timeout and ACK, before turn the packet to DFA
void TCP_connect::Resender_in(Packet *packet) {
	TCPHeader* header = header_get<TCPHeader>(packet);
	uint8_t type = header->type;
	uint16_t port = header->dst_port;
	uint32_t ackseq = header->ackseq;
	
	port_entry &p_entry = _status_table[port];
	
	assert(type != TCP_DATA && type != TCP_DATAACK);
	
	// Process ACK
	if (type == TCP_ACK || type == TCP_SYNACK) {
		if (ackseq == p_entry.wait_seq + 1 || p_entry.status == PORT_CLOSING) {
			p_entry.wait_seq = 0;
			p_entry.timer->clear();
			p_entry.packet_buf->kill();
			p_entry.packet_buf = NULL;
			DFA_From_IP(packet);
		} else {
			packet->kill();
		}
	} else {
		//Directly send
		DFA_From_IP(packet);
	}
}

// Add a timeout scheduler, before send the packet out, for TIMEOUT
void TCP_connect::Resender_out(Packet *packet) {
	TCPHeader* header = header_get<TCPHeader>(packet);
	uint8_t type = header->type;
	uint16_t port = header->src_port;
	uint32_t seq = header->seq;
	
	assert(type != TCP_DATA && type != TCP_DATAACK);
	
	port_entry &p_entry = _status_table[port];
	
	if (type != TCP_ACK) {
		// Not waiting
		//click_chatter("Wait port:%d, seq:%d", port, seq);
		assert(p_entry.wait_seq == 0 && p_entry.packet_buf == NULL);
		
		p_entry.wait_seq = seq;
		p_entry.packet_buf = PacketCopy(packet);
		p_entry.timeout_times = 0;
		p_entry.timer->clear();
		p_entry.timer->schedule_after_sec(_timeout);
	}
	
	output(0).push(packet);
}

void TCP_connect::run_timer(Timer *timer) {
	HashTable<Timer*, uint16_t>::iterator target = _timer_finder.find(timer);
	assert(target);
	
	uint16_t port = target.value();
	port_entry &p_entry = _status_table[port];
	
	// TIME_WAIT clock, thoroughly close the connection
	if (p_entry.status == PORT_TIME_WAIT) {
		p_entry.status = PORT_CLOSED;
		send_to_app(TCP_CLOSE, p_entry.packet_buf);
		p_entry.packet_buf = NULL;
		return;
	}

	// reach MAX_TIMEOUT_TIMES, drop the packet
	if ( ++p_entry.timeout_times == MAX_TIMEOUT_TIMES ) {
		p_entry.wait_seq = 0;
		p_entry.timer->clear();
		p_entry.timeout_times = 0;
		
		uint8_t &status = p_entry.status;
		if (status == PORT_SYN_RCVD) {
			status = PORT_LISTEN;
			p_entry.packet_buf->kill();
		} else if (status == PORT_SYN_SENT) {
			status = PORT_CLOSED;
			click_chatter("Fail to connect, close");
			send_to_app(TCP_CLOSE, swap_src_dst(p_entry.packet_buf));
		} else if (status != PORT_CLOSED && status != PORT_LISTEN) {
			status = PORT_CONNECT;
			p_entry.packet_buf->kill();
		}

		p_entry.packet_buf = NULL;
	} else {
		//TIMEOUT
		click_chatter("TCP: timeout to send packet with type %d in status %d", 
				header_get<TCPHeader>(p_entry.packet_buf)->type, 
				p_entry.status );
		output(0).push( PacketCopy(p_entry.packet_buf) );
		p_entry.timer->reschedule_after_sec(_timeout);
	}
}

CLICK_ENDDECLS
EXPORT_ELEMENT(TCP_connect)
