#ifndef CLICK_TCP_CONNECT_HH
#define CLICK_TCP_CONNECT_HH


#include <click/config.h>
#include <click/packet.hh> 
#include <click/timer.hh>
#include <algorithm>
#include <click/hashtable.hh>
#include "connection.hh"
#include "TCPHeader.hh"
#include "TCP_connect.hh"

const static double _timeout = 3;
const static int MAX_PORT = 256;
const static int MAX_TIMEOUT_TIMES = 10;

CLICK_DECLS

class TCP_connect : public Element {
	public:
		TCP_connect() {
		}
		~TCP_connect() {
			for (int i = 0; i < MAX_PORT; i++)
				delete _status_table[i].timer;
		}
		const char* class_name() const { return "TCP_connect"; }
		const char* port_count() const { return "1-/1-"; }
		const char* processing() const { return "h/h"; }
		
		int configure(Vector<String> &conf, ErrorHandler *errh);
		int initialize(ErrorHandler*);
		void run_timer(Timer *timer);
		void push(int port, Packet *packet);


	private:
		HashTable<Timer*, uint16_t> _timer_finder;
		int port_to_click_port[MAX_PORT + 5];
		port_entry _status_table[MAX_PORT];

		void change_status(uint16_t src_port, uint32_t dst_ip, uint32_t dst_port, uint8_t new_status);
		uint8_t get_status(uint16_t src_port);

		void send_to_ip(uint8_t type, Packet *packet, uint32_t seq, uint32_t ackseq);
		void send_to_app(uint8_t type, Packet *packet);

		void DFA_From_APP(Packet* packet);
		void DFA_From_IP(Packet* packet);
		void time_wait(int, Packet*);

		void Resender_in(Packet *packet);
		void Resender_out(Packet *packet);
};

CLICK_ENDDECLS
#endif
