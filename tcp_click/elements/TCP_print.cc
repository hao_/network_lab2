#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/packet.hh> 
#include <click/timer.hh>
#include "header_helper.cc"
#include "TCP_print.hh"

CLICK_DECLS

static String get_tcp_type(uint8_t t) {
	switch (t) {
		case 0: return "TCP_DATA"; break;
		case 1: return "TCP_DATAACk"; break;
		case 2: return "TCP_CONNECT"; break;
		case 3: return "TCP_LISTEN"; break;
		case 4: return "TCP_CLOSE"; break;
		case 5: return "TCP_SYN"; break;
		case 6: return "TCP_SYNACK"; break;
		case 7: return "TCP_ACK"; break;
		case 8: return "TCP_FIN"; break;
		case 9: return "TCP_FINACK"; break;
		case 10: return "ASK_TO_CLOSE"; break;
	}
	return "Wrong_Type";
}

static String ip_to_dot(uint32_t ip)
{
	uint8_t a[4];
	char result[20];
	for (int i = 0; i < 4; ++i)
	{
		a[i] = ip % (1 << 8);
		ip >>= 8;
	}
	result[sprintf(result, "%d.%d.%d.%d", a[3], a[2], a[1], a[0])] = 0;

	String St = String(result);
	return St;
} 

int TCP_print::configure(Vector<String> &conf, ErrorHandler *errh) {
	String src_ip, dst_ip;
	if (cp_va_kparse(conf, this, errh,
					"CONTROL_INFO", cpkM, cpUnsigned, &_control_info,
					cpEnd) < 0) {
		return -1;
	}
	if (_control_info > 1)
		return -1;
	return 0;
}

void TCP_print::push(int port, Packet *packet) {
	TCPHeader *header = header_get<TCPHeader>(packet);
	String src_ip = ip_to_dot(header->src_ip), dst_ip = ip_to_dot(header->dst_ip);

	char *payload = (char*)header_payload<TCPHeader>(packet);
	int len = packet->length() - sizeof(TCPHeader);
	char *buf = new char[len + 4];
	memcpy(buf, payload, len);
	buf[len] = 0;
	click_chatter("TCPPacket:  dst=%s:%d src=%s:%d", 
			dst_ip.c_str(), header->dst_port, src_ip.c_str(), header->src_port);
	click_chatter("Payload:    %s", buf);
	delete []buf;
	if (_control_info) {
		click_chatter("%s, rest_win: %u, seq: %u, ackseq: %u\n", get_tcp_type(header->type).c_str(), 
					header->rest_win, header->seq, header->ackseq);
	}

	output(port).push(packet);
}

CLICK_ENDDECLS 
EXPORT_ELEMENT(TCP_print)
