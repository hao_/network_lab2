#ifndef CLICK_TCP_PRINT_HH
#define CLICK_TCP_PRINT_HH

#include <click/config.h>
#include <click/packet.hh> 
#include <click/timer.hh>
#include <click/hashtable.hh>
#include "TCPHeader.hh"

CLICK_DECLS

class TCP_print : public Element {
	public:
		TCP_print() {};
		~TCP_print() {};

		const char* class_name() const { return "TCP_print"; }
		const char* port_count() const { return "1/1"; }
		const char* processing() const { return "h/h"; }

		void push(int, Packet*);
		int configure(Vector<String>&, ErrorHandler*);
	private:

		uint8_t _control_info;
};

CLICK_ENDDECLS
#endif
