#ifndef CLICK_CONNECTION_HH
#define CLICK_CONNECTION_HH

typedef enum{
	PORT_CLOSED = 0,
	PORT_LISTEN,
	PORT_SYN_RCVD,
	PORT_SYN_SENT,
	PORT_CONNECT,
	PORT_FIN1,
	PORT_FIN2,
	PORT_CLOSING,
	PORT_TIME_WAIT,
	PORT_CLOSE_WAIT,
	PORT_LAST_ACK
} port_status;

struct port_entry{
	uint8_t status;
	uint32_t dst_ip;
	uint16_t dst_port;
	
	uint32_t wait_seq;
	Timer *timer;
	Packet* packet_buf;
	Packet* conn_last_ack;
	uint8_t timeout_times;
};

#endif
