template<class Header>
static char* header_payload(Packet *packet) {
	return (char*)(packet->data() + sizeof(Header));
}

template<class Header>
static Header* header_get(Packet *packet) {
	return (Header*)(packet->data());
}

template<class Header>
static Packet* header_wrap(Packet *packet) {
	WritablePacket *wrapper = Packet::make(40, 0, sizeof(Header) + packet->length(), 28);
	memcpy( header_payload<Header>(wrapper) , packet->data(), packet->length() );
	
	return wrapper;
}

template<class Header>
static Packet* header_unpack(Packet *packet) {
	WritablePacket *payload = Packet::make(40, 0, packet->length() - sizeof(Header), 28);
	memcpy( payload->data() , header_payload<Header>(packet), payload->length() );
	return payload;
}
