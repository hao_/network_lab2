#include <string.h>
#include <click/packet.hh>

CLICK_DECLS
uint32_t translateIP(const char *s) {
	uint32_t ip = 0 , tmp = 0, len = strlen(s);
	for (uint32_t i = 0; i < len; i++)
		if (s[i] == '.') {
			ip <<= 8;
			ip += tmp;
			tmp = 0;
		} else tmp = 10 * tmp + s[i] - '0';
	ip <<= 8;
	ip += tmp;
	return ip;
}

void print_ip_dot(uint32_t ip) {
	char buf[50], *s = buf;
	while (ip) {
		s += sprintf(s, ".%u", ip % (1<<8));
		ip >>= 8;
	}
	*s = 0;
	click_chatter("%s", buf);
}
CLICK_ENDDECLS
