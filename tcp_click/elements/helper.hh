#ifndef CLICK_MYTCP_HELPER_HH
#define CLICK_MYTCP_HELPER_HH

#include <string.h>
#include <click/packet.hh>

CLICK_DECLS

uint32_t translateIP(const char *s);

void print_ip_dot(uint32_t ip);

CLICK_ENDDECLS
#endif
